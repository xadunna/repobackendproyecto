

function obtenerFecha() {
  var fecha =new Date();
  var day = fecha.getDate();
  var month = fecha.getMonth() + 1;
  var year = fecha.getFullYear();

  day = añadirCeroDelante(day);
  month = añadirCeroDelante(month);

  return day + "-" + month + "-" + year;

}

function añadirCeroDelante(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}

function esVacio(campo){
  if(campo == null){
    return true;
  }else{
    return false;
  }
}
module.exports.obtenerFecha = obtenerFecha;
module.exports.esVacio = esVacio;
