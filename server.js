//Carga la librería de de variables de entorno y carga el fichero .env
require('dotenv').config();
//fija variable que no puede cambiar; el require mira en las dependencias
const express = require('express');
//importo nuevo paquete
const bodyParser = require('body-parser');


const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const transactionController = require('./controllers/TransactionController');
const currencyConverterController = require('./controllers/CurrencyConverterController');

//inicializa el FWK y lo pone en la variable
const app = express();

//permite que sólo lleguen peticiones de un navegador el mismo dominio
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 // This will be needed. Por defecto no deja pasar las cabeceras. Para tsec token jwt hay que añadir
 res.set("Access-Control-Allow-Headers", "Content-Type, Authorization");


 next();
}



//Indicamos a express que loss bodys van a ir en JSON(asignamos un preprocesador)
app.use(express.json());
app.use(enableCORS);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
//app.use(expressJwt({secret:process.env.TOKEN}).unless({path: ["/backend_banco/login"]}));

//si no está la variable de entorno asigna el puerto 3000
const port = process.env.PORT || 3000;
//inicializa la app
app.listen(port);

console.log("API escuchando en el puerto " + port);



  //recibimos filtros

app.get ("/backend_banco/users/:user_id",authController.verificarToken,userController.getUserById);
app.post("/backend_banco/users",userController.createUser);
app.delete("/backend_banco/users/:user_id",authController.verificarToken,userController.deleteUser);
app.post ("/backend_banco/login",authController.login);
app.post ("/backend_banco/logout/:user_id",authController.verificarToken,authController.logout);
app.get("/backend_banco/accounts/:user_id",authController.verificarToken,accountController.getAccountByUserId);
app.post("/backend_banco/accounts",authController.verificarToken,accountController.createAccount);
app.delete("/backend_banco/accounts/:account_id",authController.verificarToken,transactionController.deleteTransactionsByAccountID,accountController.deleteAccount);
app.get("/backend_banco/transactions/:account_id",authController.verificarToken,transactionController.getTransactionByAccountId);
app.post("/backend_banco/transactions",authController.verificarToken,transactionController.createTransaction);
app.get("/backend_banco/currencies",authController.verificarToken,currencyConverterController.getCurrencies);
app.get("/backend_banco/currencies/:curr_from/:curr_to/:amount",authController.verificarToken,currencyConverterController.getCurrencyConversion);
