//importo el paquete
const crypt = require('../crypt');
const requestJson = require('request-json');
const utils = require('../utils');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechummt12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUserById(req,res){
  console.log("GET /backend_banco/users/:user_id");
  if(isNaN(req.params.user_id)){
    console.log("Parámetro no numérico");
    res.status(400).send({"msg" :"Parámetro no numérico" });
  } else {
    var userId = Number.parseInt(req.params.user_id);
    console.log("La id del usuario a obtener es: " + userId);

    var query = "q=" + JSON.stringify({"id": userId});
    var httpClient =requestJson.createClient(baseMLABUrl);
    console.log("Client created");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body) {
    var response;
      if(err){
        console.log("Error obteniendo usuario");
        response = {"msg": "Error obteniendo usuario"};
        res.status(500);
      } else{
        if(body.length >0){
          response = body[0];
        }else{
          console.log("Usuario no encontrado");
          response = {"msg" : "Usuario no encontrado"};
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}
}


function createUser(req,res){
  console.log("POST /backend_banco/users");
  if(utils.esVacio(req.body.first_name) || utils.esVacio(req.body.last_name) ||utils.esVacio(req.body.email) || utils.esVacio(req.body.password)){
    console.log("Campos vacíos en el cuerpo del mensaje");
    res.status(400).send({"msg" :"Campos vacíos en el cuerpo del mensaje" });
  }  else {
    //buscamos el id maximo
    var query ="f=" + JSON.stringify({"id": 1,"_id" : 0});
    query += "&s=" + JSON.stringify({"id":-1});
    query += "&l=1";
    var httpClient =requestJson.createClient(baseMLABUrl);
    console.log("Client created");


    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err,resMLab, body) {
        if(err){
          console.log ("Error obteniendo id máximo de usuario");
          res.status(500).send({"msg": "Error obteniendo id máximo de usuario"});
        } else{
            var newId = 1;
            if(body.length >0){
              newId = Number.parseInt(body[0].id) + 1;
            }
              console.log("id Usuario: " + newId);
              var newUser ={
                "id" : newId,
                "first_name": req.body.first_name,
                "last_name": req.body.last_name,
                "email": req.body.email,
                "password": crypt.hash(req.body.password)
              };
              httpClient.post("user?" + mLabAPIKey,newUser,
              function(errPost,resMLabPost, bodyPost) {
                if(errPost) {
                  console.log ("Error creando usuario");
                  res.status(500).send({"msg": "Error creando usuario"});
                }else{
                  console.log("Usuario creado id: " + newId);
                  res.status(201).send({  "msg": "Usuario creado","id": newId});
                }//else errPost
              }//function post
            );//post
        }//!err get
      }//funtion get
    ); //Get
  } //Chequeo body
}

function deleteUser(req,res){
  console.log("DELETE /backend_banco/users/:user_id");
  if(isNaN(req.params.user_id)){
    console.log("Parámetro no numérico");
    res.status(400).send({"msg" :"Parámetro no numérico" });
  }else {
    var userId = Number.parseInt(req.params.user_id);
    var query ="q=" + JSON.stringify({"id": userId});
    var emptyObject = [];

    var httpClient =requestJson.createClient(baseMLABUrl);
    var response;
    console.log("Client created");
    httpClient.put("user?" + query + "&" + mLabAPIKey,emptyObject,
    function(errUser,resMLabUser, bodyUser) {
      if(errUser){
        res.status(500);
        console.log("Error borrando usuario");
        response = {"msg": "Error borrando usuario"};
      } else{
        if(Number.parseInt(bodyUser.removed)>0){
          response = {"msg": "Usuario borrado"};
          console.log("Usuario borrado");
        }else {
          res.status(404);
          response = {"msg": "Usuario no encontrado"};
          console.log("Usuario no encontrado");
      }
    }
    res.send(response);
  }
  );
}

}


// function deleteUser(req,res){
//   console.log("DELETE /backend_banco/users/:user_id");
//
//   var userId = Number.parseInt(req.params.user_id);
//   var query ="q=" + JSON.stringify({"id": userId});
//   var emptyObject = [];
//   var errorBorrandoUsuario = false;
//
//   var httpClient =requestJson.createClient(baseMLABUrl);
//   var response;
//   console.log("Client created");
//   httpClient.get("account?" + query + "&" + mLabAPIKey,
//   function(err,resMLab, body) {
//     if(err){
//       errorBorrandoUsuario = true;
//       response = {"msg": "Error obteniendo cuentas"};
//       console.log("Error obteniendo cuentas");
//     } else{
//       if(body.length >0){
//         var listaCuentas = body;
//         //por cada cuenta borramos los movimientos
//         for(cuenta of listaCuentas){
//           query = "q=" + JSON.stringify({"IBAN": cuenta.IBAN});
//           //primero borramos los movimientos asociados a la cuenta
//           httpClient.put("transaction?" + query + "&" + mLabAPIKey,emptyObject,
//           function(err,resMLab, body) {
//             if(err){
//                 errorBorrandoUsuario = true;
//             } else{
//
//           }//else borrado movimientos
//         }//function borrado movimientos
//       );//put acounts
//         } //for
//         //borramos las cuentas para ese usuario
//         query = "q=" + JSON.stringify({"id": userId});
//         //borramos las cuentas para ese usuario
//         httpClient.put("account?" + query + "&" + mLabAPIKey,emptyObject,
//         function(errAccount,resMLabAccount, bodyAccount) {
//           console.log("Borrando cuentas");
//           if(errAccount){
//             errorBorrandoUsuario = true;
//             console.log("Error borrando cuentas del usuario");
//             response = {"msg": "Error borrando cuentas del usuario"};
//           } else{
//             console.log("Cuentas del usuario borradas");
//           }
//       }//function delete account
//       );//Delete account
//       } //get accounts body
//       //no tiene cuentas; borramos usuario
//       httpClient.put("user?" + query + "&" + mLabAPIKey,emptyObject,
//       function(errUser,resMLabUser, bodyUser) {
//         if(errUser){
//           errorBorrandoUsuario = true;
//           console.log("Error borrando usuario");
//           response = {"msg": "Error borrando usuario"};
//         } else{
//           console.log("Usuario borrado: " + Number.parseInt(body.removed));
//           if(Number.parseInt(body.removed)>0){
//             response = {"msg": "Usuario borrado"};
//             console.log("Usuario borrado");
//           }else {
//             response = {"msg": "Usuario no encontrado"};
//             console.log("Usuario no encontrado");
//           }
//
//         }
//         res.send(response);
//       }
//     );
//     }//else get accounts
//
//   } //function get cuentas
// );//get accounts
//
// res.status = (errorBorrandoUsuario)?500:200;
// res.send(response);
//
// }
//para poder llamar a las fuciones desde otros ficheros

module.exports.getUserById = getUserById;
module.exports.createUser = createUser;
module.exports.deleteUser = deleteUser
