//importo el paquete
const crypt = require('../crypt');
const requestJson = require('request-json');
const utils = require('../utils');
const jwt = require('jsonwebtoken');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechummt12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const TOKEN = process.env.TOKEN;


function login(req,res){
  console.log("POST /backend_banco/login");
 if(utils.esVacio(req.body.email) || utils.esVacio(req.body.password)){
    console.log("Campos vacíos en el cuerpo del mensaje");
    res.status(400).send({"msg" :"Campos vacíos en el cuerpo del mensaje" });
  } else{
    var email = req.body.email;
    var password = req.body.password;

    var httpClient =requestJson.createClient(baseMLABUrl);
    console.log("Client created");

    //me aseguro que no metan código
    var query = "q=" + JSON.stringify({"email" : email});

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body) {
      if(body.length >0){
        //Compruebo la contraseña
        var response = body[0];
        var passwordEncripted=body[0].password;
        if(crypt.checkPassword(password,passwordEncripted) == true){
          console.log("Contraseña correcta");
          var id = Number.parseInt(body[0].id);
          query = "q=" + JSON.stringify({"id" : id});
          //POdemos haer la consulta por id en vez de por emial con un parseInt de los params y un stringify
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPut,resMLabPut, bodyPut) {
            if(errPut){
              console.log ("Error actualizando login de usuario");
              res.status(500).send({"msg": "Error actualizando login de usuario"});
            }else{
              // create a token
              var token = jwt.sign({ id: id }, TOKEN); // el token expira en 1 hora
              var response = {
                "msg": "Login correcto",
                "id": body[0].id,
                "token": token
              }
              res.send(response);
            }
          }
        );
       }else{
         console.log("Contraseña incorrecta");
         res.status(401).send({"msg" : "Contraseña incorrecta"});
       }
      }else{
        console.log("Usuario inexistente");
        res.status(404).send({"msg" : "Usuario inexistente"});
      }
    }
  ); //get
}//else
}


function logout(req,res){
  console.log("POST /backend_banco/logout");
  if(isNaN(req.params.user_id)){
    console.log("Parámetro no numérico");
    res.status(400).send({"msg" :"Parámetro no numérico" });
  }else{
    var httpClient = requestJson.createClient(baseMLABUrl);
    console.log("Client created");
    var userId = Number.parseInt(req.params.user_id);
    var query = "q=" + JSON.stringify({"id": userId});

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body) {
      if(err){
        console.log ("Error actualizando logout de usuario");
        res.status(500).send({"msg": "Error actualizando logout de usuario"});
      }else{
        if(body.length >0){
          if(body[0].logged === true){
            console.log("Deslogueamos al usuario");
            var putBody = '{"$unset":{"logged":""}}';
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPut,resMLabPut, bodyPut) {
                res.send({"msg" : "Logout correcto"});
            }
            );
          }else{
            console.log("Logout incorrecto");
            res.status(401).send({"msg" : "Logout incorrecto"});
          }
      } else{
        console.log("Usuario inexistente");
        res.status(404).send({"msg" : "Usuario inexistente"});
      }
      }
    }
  );
}
}

function verificarToken(req, res, next) {
  console.log("Verificamos token");
  // decode token
  if (req.headers.authorization) {
    var token = req.headers.authorization.split(" ")[1];
    // verifies secret and checks exp
    jwt.verify(token, TOKEN, function(err, decoded) {
      if (err) {
        console.log("Token inválido");
        res.status(403).send({ "msg" : "Token inválido" });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {
    // if there is no token
    // return an error
    console.log("No hay token");
    return res.status(400).send({"msg" : "No hay token" });

  }
}
//para poder llamar a las fuciones desde otros ficheros
module.exports.login = login;
module.exports.logout = logout;
module.exports.verificarToken = verificarToken;
