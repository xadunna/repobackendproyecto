//importo el paquete
const requestJson =require('request-json');

const baseCCUrl = "https://free.currconv.com/api/v7/";
const mCCAPIKey = "apiKey=" + process.env.CC_API_KEY;

function getCurrencies(req,res){
  console.log("GET /backend_banco/currencies");
  var httpClient = requestJson.createClient(baseCCUrl);
  console.log("Client created");
  httpClient.get("currencies?" + mCCAPIKey,
  function(err,resMLab, body) {
    if(err){
      console.log("Error obteniendo monedas");
      var response = {
        "msg": "Error obteniendo monedas"
      }
      res.status(500);
    } else{
      if(body.results != null){
        var jsonResult = body.results;
        var jsonCurrency;
        var jsonCurrencyArray = [];

        var jsonKeys = Object.keys(jsonResult);
        jsonKeys.sort();
        jsonKeys.forEach(function(key){
        jsonCurrency = {
            "currency" : key,
            "currencyName" : jsonResult[key].currencyName
          }
          jsonCurrencyArray.push(jsonCurrency);

        });
        var response = jsonCurrencyArray;
      }else{
        console.log("No hay monedas");
        var response = {"msg" : "No hay monedas"};
        res.status(404);
      }
    }
    res.send(response);
  }
);
}
function  getCurrencyConversion(req, res){
console.log("GET /backend_banco/currencies/:curr_from/:curr_to/:amount");
if(isNaN(req.params.amount)){
  console.log("Parámetro no numérico");
  res.status(400).send({"msg" :"Parámetro no numérico" });
} else {
  var currencyFrom = req.params.curr_from;
  var currencyTo = req.params.curr_to;
  var amount = Number.parseFloat(req.params.amount).toFixed(2);
  var httpClient = requestJson.createClient(baseCCUrl);
  console.log("Client created");
  var query = "q=" + currencyFrom + "_" + currencyTo ;
  httpClient.get("convert?" + query + "&compact=ultra&" + mCCAPIKey,
  function(err,resMLab, body) {
    if(err){
      console.log("Error convirtiendo monedas");
      var response = {
        "msg": "Error convirtiendo monedas"
      }
      res.status(500);
    } else{
        var jsonResult = body;
        if( jsonResult.length == 0){
          console.log("No hay cambio para esas divisas");
          var response = {"msg" : "No hay cambio para esas divisas"};
          res.status(404);
        }else{

          var val = jsonResult[currencyFrom + "_" + currencyTo];
          if (val) {
            var total = (val * amount).toFixed(6);
            var response = {"cambio" : total};
          } else {
            console.log("Cambio vacío");
            var response = {"msg" : "Cambio vacío"};
            res.status(404);
          }
      }
    }
    res.send(response);
  }
  );
}
}

//para poder llamar a las fuciones desde otros ficheros
module.exports.getCurrencies = getCurrencies;
module.exports.getCurrencyConversion = getCurrencyConversion;
