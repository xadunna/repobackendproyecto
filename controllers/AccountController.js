//importo el paquete
const crypt = require('../crypt');
const requestJson =require('request-json');
const utils = require('../utils');
const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechummt12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;





function getAccountByUserId(req,res){
  console.log("GET /backend_banco/accounts/:user_id");
  if(isNaN(req.params.user_id)){
    console.log("Parámetro no numérico");
    res.status(400).send({"msg" :"Parámetro no numérico" });
  } else{
    var userId = Number.parseInt(req.params.user_id);

    var query = "q=" + JSON.stringify({"id_usuario": userId});
    var httpClient = requestJson.createClient(baseMLABUrl);
    console.log("Client created");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body) {
      var response;
      if(err){
        console.log("Error obteniendo cuentas");
        response = {"msg": "Error obteniendo cuentas"};
        res.status(500);
      } else{
        if(body.length >0){
          response = body;
        }else{
          console.log("El usuario no tiene cuentas")
          response = {"msg" : "El usuario no tiene cuentas"};
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}
}

function createAccount(req,res){
  console.log("POST /backend_banco/accounts");
  if(utils.esVacio(req.body.id_usuario) || utils.esVacio(req.body.iban)){
    console.log("Campos vacíos en el cuerpo del mensaje");
    res.status(400).send({"msg" :"Campos vacíos en el cuerpo del mensaje" });
  }  else
  if (isNaN(req.body.id_usuario)){
    console.log(req.body.id_usuario);
    console.log("Campo id_usuario no numérico");
    res.status(400).send({"msg" :"Campo id_usuario no numérico" });
  }  else {
    var iban = req.body.iban;
    var id_usuario = Number.parseInt(req.body.id_usuario);

    var query ="f=" + JSON.stringify({"id": 1,"_id" : 0});
    query += "&s=" + JSON.stringify({"id":-1});
    query += "&l=1";

    var httpClient = requestJson.createClient(baseMLABUrl);
    console.log("Client created");
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err,resMLab, body) {
        if(err){
          console.log ("Error obteniendo id máximo de cuenta");
          res.status(500).send({"msg": "Error obteniendo id máximo de cuenta"});
        } else{
            var newId = 1;
            if(body.length >0){
              newId = Number.parseInt(body[0].id) + 1;
            }
            console.log("id cuenta: " + newId);
            var newAccount = {
                "id" : newId,
                "IBAN" : iban,
                "id_usuario" : id_usuario,
                "balance" : 0
              };
              httpClient.post("account?" + mLabAPIKey,newAccount,
              function(errPost,resMLabPost, bodyPost) {
                if(errPost){
                  console.log("Error creando cuenta");
                  res.status(500).send({"msg" : "Error creando cuenta"});
                } else{
                  console.log("Cuenta creada");
                  res.status(201).send({"msg" : "Cuenta creada", "id" : newId});
                }
              } //function post
            ); //post
        }//!err get
      }//funtion get
    ); //Get
  } //chequeo body
}


function deleteAccount(req,res){
  console.log("DELETE /backend_banco/accounts/:account_id");
  if(isNaN(req.params.account_id)){
    console.log("Parámetro no numérico");
    res.status(400).send({"msg" :"Parámetro no numérico" });
  }else {
    var accountId = Number.parseInt(req.params.account_id);

    var httpClient = requestJson.createClient(baseMLABUrl);
    var query = "q=" + JSON.stringify({"id": accountId});
    var emptyObject = [];

    //primero borramos los movimientos asociados a la cuenta
    httpClient.put("account?" + query + "&" + mLabAPIKey,emptyObject,
    function(errAccount,resMLabAccount, bodyAccount) {
      console.log("Borrando cuenta");
      if(errAccount){
        console.log("Error borrando cuenta");
        res.status(500).send({"msg": "Error borrando cuenta"});
      } else{
        if(Number.parseInt(bodyAccount.removed) > 0){
          console.log("Cuenta borrada");
          res.send({"msg": "Cuenta borrada"});
        }else{
          console.log("No existe la cuenta");
          res.status(404).send({"msg": "No existe la cuenta"});
        }
      }//else delete account
  }//function delete account
  );//Delete account
}
}

// function deleteAccount(req,res){
//   console.log("DELETE /backend_banco/accounts/:account_id");
//   if(isNaN(req.params.account_id)){
//     console.log("Parámetro no numérico");
//     res.status(400).send({"msg" :"Parámetro no numérico" });
//   }else {
//     var accountId = Number.parseInt(req.params.account_id);
//
//     var httpClient = requestJson.createClient(baseMLABUrl);
//     var query = "q=" + JSON.stringify({"id_cuenta": accountId});
//     var emptyObject = [];
//
//     //primero borramos los movimientos asociados a la cuenta
//     httpClient.put("transaction?" + query + "&" + mLabAPIKey,emptyObject,
//     function(err,resMLab, body) {
//       if(err){
//         console.log("Error borrando movimientos");
//         res.status(500).send({"msg": "Error borrando movimientos"});
//       } else{
//         //retorno del borrado  { n: 0, removed: 1 }
//         console.log("Movimientos borrados: " + Number.parseInt(body.removed));
//         //borramos la cuenta
//         query = "q=" + JSON.stringify({"id": accountId});
//         httpClient.put("account?" + query + "&" + mLabAPIKey,emptyObject,
//         function(errAccount,resMLabAccount, bodyAccount) {
//           console.log("Borrando cuenta");
//           if(errAccount){
//             console.log("Error borrando cuenta");
//             res.status(500).send({"msg": "Error borrando cuenta"});
//           } else{
//             if(Number.parseInt(bodyAccount.removed) >0){
//               console.log("Cuenta borrada");
//               res.send({"msg": "Cuenta borrada"});
//             }else{
//               console.log("No existe la cuenta");
//               res.status(404).send({"msg": "No existe la cuenta"});
//             }
//           }//else delete account
//       }//function delete account
//       );//Delete account
//       }
//     }
//   );
// }
// }


//para poder llamar a las fuciones desde otros ficheros

module.exports.getAccountByUserId = getAccountByUserId;
module.exports.createAccount = createAccount;
module.exports.deleteAccount = deleteAccount;
