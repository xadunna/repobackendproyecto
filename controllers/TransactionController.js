//importo el paquete
const requestJson = require('request-json');
const utils = require('../utils');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechummt12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const ingreso = "INGRESO";
const reintegro = "REINTEGRO";


function createTransaction(req,res){
  console.log("POST /backend_banco/transactions");
  if(utils.esVacio(req.body.id_cuenta) || utils.esVacio(req.body.tipo_movimiento) || utils.esVacio(req.body.importe) ){
    console.log("Campos vacíos en el cuerpo del mensaje");
    res.status(400).send({"msg" :"Campos vacíos en el cuerpo del mensaje" });
  } else if (isNaN(req.body.importe)){
    console.log("Campo importe no numérico");
    res.status(400).send({"msg" :"Campo importe no numérico" });
  } else {

      var accountId = req.body.id_cuenta;
      var tipoMovimiento = req.body.tipo_movimiento;
      var importe = Number.parseFloat(req.body.importe);
      var descripcion = req.body.descripcion;

      //Revisamos que el tipo de movimiento sea correcto

      if(tipoMovimiento == ingreso || tipoMovimiento == reintegro) {
        //Debemos recuperar la cuenta para poder actualizar el balance
        var httpClient = requestJson.createClient(baseMLABUrl);


        var query = "q=" + JSON.stringify({"id" : accountId});
        httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(errGet,resMLab, bodyGet) {
            console.log("Recuperando cuenta");
          if(errGet){
              console.log("Error recuperando cuenta");
              res.status(500).send({"msg" : "Error recuperando cuenta"});
          }else{
            if (bodyGet.length > 0) {
            //hemos recuperado la cuenta
            console.log("Cuenta recuperada");
            var balance = Number.parseFloat(bodyGet[0].balance);
            //console.log("balance:" + balance);
            console.log("importe:"+ importe);

            var saldoInicial = balance;
            //Actualizamos el balance, según el importe
            if(tipoMovimiento == ingreso){
              balance = balance + importe;
            }else {
              balance = balance - importe;
            }
            var saldoFinal = balance;
            //console.log("saldoFinal:" + Number.parseFloat(saldoFinal).toFixed(2));
            //Actualizamos el balance en la información de la cuenta
            //console.log(query);
            var putBody = '{"$set":{"balance":' + Number.parseFloat(saldoFinal).toFixed(2)+'}}';
            //console.log(JSON.parse(putBody));
            httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPut,resMLabPut, bodyPut) {
              if(errPut){
                console.log("Error actualizando saldo");
                res.status(500).send({"msg" : "Error actualizando saldo"});
              }
              else{
                console.log("Balance actualizado");
                //Añadimos el movimiento

                var newTransaction = {
                  "tipo_movimiento" : tipoMovimiento,
                  "fecha": utils.obtenerFecha(),
                  "importe": (tipoMovimiento==reintegro)? importe * -1:importe,
                  "id_cuenta": accountId,
                  "saldo_inicial": Number.parseFloat(saldoInicial).toFixed(2),
                  "saldo_final": Number.parseFloat(saldoFinal).toFixed(2),
                  "descripcion": descripcion
                }
                httpClient.post("transaction?" + mLabAPIKey,newTransaction,
                function(errPost,resMLabPost, bodyPost) {
                  if(errPost) {
                    console.log("Error creando transacción");
                    res.status(500).send({"msg" : "Error creando transacción"});
                  }else{
                    console.log("Transacción creada");
                    res.status(201).send({"msg" : "Transacción creada"});
                }
                } //function post
              ); //post
            } //else actualizar balance
          }//funcion pust
          );//put
        }  else{
            console.log("No existe la cuenta");
            res.status(404).send({"msg" : "No existe la cuenta"});
        }//else recuperar cuenta
        } //body get account
        }//function get
      );//recuperar cuenta
      }else{
        console.log("Tipo de movimiento incorrecto");
        res.status(500).send({"msg" : "Error en tipo de movimiento"});
      }//Chequeamos tipo de movimiento
    }
}

function getTransactionByAccountId(req,res){
  console.log("GET /backend_banco/transactions/:account_id");
  if(isNaN(req.params.account_id)){
    console.log("Parámetro no numérico");
    res.status(400).send({"msg" :"Parámetro no numérico" });
  } else{
    var accountId = Number.parseInt(req.params.account_id);
    var query = "q=" + JSON.stringify({"id_cuenta": accountId});
    var httpClient = requestJson.createClient(baseMLABUrl);


    httpClient.get("transaction?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body) {
      console.log("Recuperando movimientos");
      if(err){
        console.log("Error obteniendo movimientos");
        var response = {
          "msg": "Error obteniendo movimientos"
        }
        res.status(500);
      } else{
        if(body.length >0){
          var response = body;

        }else{
          console.log("La cuenta no tiene movimientos");
          var response = {
            "msg" : "La cuenta no tiene movimientos"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}// else chequeo campos
}


function deleteTransactionsByAccountID(req, res,next){
  console.log("DELETE /backend_banco/transactions/:account_id");
  if(isNaN(req.params.account_id)){
    console.log("Parámetro no numérico");
    res.status(400).send({"msg" :"Parámetro no numérico" });
  } else {
    var accountId = Number.parseInt(req.params.account_id);
    var query ="q=" + JSON.stringify({"id_cuenta": accountId});
    var emptyObject = [];

    var httpClient =requestJson.createClient(baseMLABUrl);

    console.log("Client created");

    httpClient.put("transaction?" + query + "&" + mLabAPIKey,emptyObject,
    function(err,resMLab, body) {
      if(err){
        console.log("Error borrando movimientos");
        res.status(500).send({"msg": "Error borrando movimientos"});
      } else{
        //retorno del borrado  { n: 0, removed: 1 }
        if(Number.parseInt(body.removed) > 0){
          console.log("Movimientos borrados " + Number.parseInt(body.removed));
        }else{
          console.log("No existen movimientos para esa cuenta");
        }
        next();
      }

    }
  );
}//else chequeo campos
}


//para poder llamar a las fuciones desde otros ficheros

module.exports.getTransactionByAccountId = getTransactionByAccountId;
module.exports.createTransaction = createTransaction;
module.exports.deleteTransactionsByAccountID= deleteTransactionsByAccountID;
