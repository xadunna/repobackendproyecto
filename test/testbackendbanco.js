const mocha = require('mocha');
const chai  = require('chai');
const chaihttp = require('chai-http');
const url='http://localhost:3000';
//para que podamos usar chait-http desde chai
chai.use(chaihttp);

//mecanismo para hacer aserciones
var should = chai.should();
var newUserId;
var newAccountId;
var token;

describe("Test de API de usuarios: creación de usuarios",
function() {
  it('Test que crea usuario con el body vacío', function(done){
      chai.request(url)
      .post('/backend_banco/users')
      .send({})
      .end(
        function(err, res){
          // //función manejadora: es donde irán las aserciones
          console.log("Request finished");
          //chequeo el status de la respuesta
          res.should.have.status(400);
          //chequeo que el objeto sea un json
          res.body.should.be.an('object');
          res.body.should.have.property('msg');
          done(); // momento en el que hay que comprobar las aserciones
        }
      )
    }
    )
  it('Test que crea usuario de test', function(done){
    chai.request(url)
    .post('/backend_banco/users')
    .send({"first_name": "prueba", "last_name": "prueba", "email": "email@email.com", "password": "password"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(201);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        res.body.should.have.property('msg');
        res.body.should.have.property('id');
        newUserId = Number.parseInt(res.body.id);
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)

describe("Test de API de autorizaciones: login de usuario",
function() {
  it('Test que hace login con body vacío', function(done){
    chai.request(url)
    .post('/backend_banco/login')
    .send({})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que hace login con usuario test y contraseña incorrecta', function(done){
    chai.request(url)
    .post('/backend_banco/login')
    .send({"email": "email@email.com", "password": "pass"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(401);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que hace login con usuario inexistente', function(done){
    chai.request(url)
    .post('/backend_banco/login')
    .send({"email": "test@test.com", "password": "pass"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que hace login con usuario test', function(done){
    chai.request(url)
    .post('/backend_banco/login')
    .send({"email": "email@email.com", "password": "password"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        res.body.should.have.property('msg');
        res.body.should.have.property('id');
        res.body.should.have.property('token');
        token = "Bearer " + res.body.token;
        newUserId = Number.parseInt(res.body.id);
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)


describe("Test de API de autorizaciones: búsqueda  de usuario",
function() {
  it("Test que busca usuario no numérico: aaa" , function(done){
    chai.request(url)
    .get('/backend_banco/users/aaa')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca usuario inexistente: 0', function(done){
    chai.request(url)
    .get('/backend_banco/users/0')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        res.body.should.have.property('msg');

        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que busca usuario test sin token", function(done){
    chai.request(url)
    .get('/backend_banco/users/'+ newUserId)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que busca usuario test con token inválido", function(done){
    chai.request(url)
    .get('/backend_banco/users/'+ newUserId)
    .set('Authorization',token + 'a')
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(403);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que busca usuario test", function(done){
    chai.request(url)
    .get('/backend_banco/users/'+ newUserId)
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        var user = res.body;
        //Comprobamos la integridad del usuario
        user.should.have.property('id');
        user.should.have.property('first_name');
        user.should.have.property('last_name');
        user.should.have.property('email');
        user.should.have.property('password');

        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)

describe("Test de API de cuentas: creación de cuentas",
function() {
  it('Test que crea cuenta con body vacío', function(done){
    chai.request(url)
    .post('/backend_banco/accounts')
    .set('Authorization',token)
    .send({})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea cuenta para un usuario no numérico', function(done){
    chai.request(url)
    .post('/backend_banco/accounts')
    .set('Authorization',token)
    .send({"iban": "CUENTA_TEST", "id_usuario": "aaa"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea  CUENTA_TEST para usuario test sin token', function(done){
    chai.request(url)
    .post('/backend_banco/accounts')
    .send({"iban": "CUENTA_TEST", "id_usuario": newUserId})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea CUENTA_TEST para usuario test con token inválido', function(done){
    chai.request(url)
    .post('/backend_banco/accounts')
    .set('Authorization',token + 'a')
    .send({"iban": "CUENTA_TEST", "id_usuario": newUserId})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(403);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea CUENTA_TEST para usuario test', function(done){
    chai.request(url)
    .post('/backend_banco/accounts')
    .set('Authorization',token)
    .send({"iban": "CUENTA_TEST", "id_usuario": newUserId})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(201);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        res.body.should.have.property('id');
        newAccountId=Number.parseInt(res.body.id);
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)

describe("Test de API de cuentas: búsqueda de cuentas",
function() {
  it('Test que busca cuentas para un usuario no numérico: aaa', function(done){
    chai.request(url)
    .get('/backend_banco/accounts/aaa')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca cuentas para un usuario inexistente: 0', function(done){
    chai.request(url)
    .get('/backend_banco/accounts/0')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca cuentas para un usuario test sin token', function(done){
    chai.request(url)
    .get('/backend_banco/accounts/' + newUserId)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que la respuesta sea un array
        res.body.should.be.an('object');
        //Comprobamos la integridad de las cuentas
        res.body.should.have.property("msg");
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca cuentas para un usuario test con token inválido', function(done){
    chai.request(url)
    .get('/backend_banco/accounts/' + newUserId)
    .set('Authorization',token + 'a')
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(403);
        //chequeo que la respuesta sea un array
        res.body.should.be.an('object');
        //Comprobamos la integridad de las cuentas
        res.body.should.have.property("msg");
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca cuentas para usuario test', function(done){
    chai.request(url)
    .get('/backend_banco/accounts/' + newUserId)
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que la respuesta sea un array
        res.body.should.be.a('array');
        //Comprobamos la integridad de las cuentas
        for (account of res.body){
          account.should.have.property('IBAN');
          account.should.have.property('id_usuario');
          account.should.have.property('id');
          account.should.have.property('balance');
        }
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)



describe("Test de API de transacciones: creación de transacciones",
function() {
  it('Test que crea una transacción con un body vacío', function(done){
    chai.request(url)
    .post('/backend_banco/transactions')
    .set('Authorization',token)
    .send({})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea una transacción con un importe no numérico', function(done){
    chai.request(url)
    .post('/backend_banco/transactions')
    .set('Authorization',token)
    .send({"id_cuenta": newAccountId, "tipo_movimiento": "INGRESO", "importe": "aaa","descripcion": "ingreso prueba"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea una transacción con un tipo de movimiento incorrecto', function(done){
    chai.request(url)
    .post('/backend_banco/transactions')
    .set('Authorization',token)
    .send({"id_cuenta": newAccountId, "tipo_movimiento": "OTRO", "importe": 20.05,"descripcion": "ingreso prueba"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(500);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea una transacción para una cuenta inexistente', function(done){
    chai.request(url)
    .post('/backend_banco/transactions')
    .set('Authorization',token)
    .send({"id_cuenta": 0, "tipo_movimiento": "INGRESO", "importe": 20.05,"descripcion": "ingreso prueba"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea una transacción para CUENTA_TEST sin token', function(done){
    chai.request(url)
    .post('/backend_banco/transactions')
    .send({"id_cuenta": newAccountId, "tipo_movimiento": "INGRESO", "importe": 20.05,"descripcion": "ingreso prueba"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea una transacción para CUENTA_TEST con token inválido', function(done){
    chai.request(url)
    .post('/backend_banco/transactions')
    .set('Authorization',token + 'a')
    .send({"id_cuenta": newAccountId, "tipo_movimiento": "INGRESO", "importe": 20.05,"descripcion": "ingreso prueba"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(403);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que crea una transacción para CUENTA_TEST', function(done){
    chai.request(url)
    .post('/backend_banco/transactions')
    .set('Authorization',token)
    .send({"id_cuenta": newAccountId, "tipo_movimiento": "INGRESO", "importe": 20.05,"descripcion": "ingreso prueba"})
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(201);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)


describe("Test de API de transacciones: búsqueda de transacciones",
function() {
  it('Test que busca todas las transacciones para una cuenta no numérica: aaa ', function(done){
    chai.request(url)
    .get('/backend_banco/transactions/aaa')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca todas las transacciones para una cuenta sin transacciones 1' , function(done){
    chai.request(url)
    .get('/backend_banco/transactions/0')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
          //Comprobamos la integridad de las transacciones
        res.body.should.have.property("msg");
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca todas las transacciones para CUENTA_TEST sin token', function(done){
    chai.request(url)
    .get('/backend_banco/transactions/' + newAccountId)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos la integridad de las transacciones
        res.body.should.have.property("msg");

        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca todas las transacciones para CUENTA_TEST con token inválido', function(done){
    chai.request(url)
    .get('/backend_banco/transactions/' + newAccountId)
    .set('Authorization',token + 'a')
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(403);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos la integridad de las transacciones
        res.body.should.have.property("msg");
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que busca todas las transacciones para CUENTA_TEST', function(done){
    chai.request(url)
    .get('/backend_banco/transactions/' + newAccountId)
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que devuelve un objeto
        res.body.should.be.a('array');
          //Comprobamos la integridad de las transacciones
        for (transaction of res.body){
          transaction.should.have.property('id_cuenta');
          transaction.should.have.property('tipo_movimiento');
          transaction.should.have.property('importe');
          transaction.should.have.property('saldo_inicial');
          transaction.should.have.property('saldo_final');
          transaction.should.have.property('descripcion');
        }
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)

describe("Test de API de cuentas: borrado de cuentas",
function() {
  it('Test que borra cuenta para un id no numérico: aaa', function(done){
    chai.request(url)
    .delete('/backend_banco/accounts/aaa')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que borra cuenta inexistente: 0', function(done){
    chai.request(url)
    .delete('/backend_banco/accounts/0')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que borra CUENTA TEST sin token', function(done){
    chai.request(url)
    .delete('/backend_banco/accounts/'+ newAccountId)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que borra CUENTA TEST con token inválido', function(done){
    chai.request(url)
    .delete('/backend_banco/accounts/'+ newAccountId)
    .set('Authorization',token + 'a')
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(403);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Test que borra CUENTA TEST', function(done){
    chai.request(url)
    .delete('/backend_banco/accounts/'+ newAccountId)
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)

describe("Test de API de autorizaciones: logout de usuario",
function() {
  it("Test que hace logout de un usuario no numérico: aaa", function(done){
    chai.request(url)
    .post('/backend_banco/logout/aaa')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que hace logout de un usuario no inexistente: 1000", function(done){
    chai.request(url)
    .post('/backend_banco/logout/1000')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que hace logout de un usuario que no está logueado: 1", function(done){
    chai.request(url)
    .post('/backend_banco/logout/1')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(401);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que hace logout de usuario test sin token", function(done){
    chai.request(url)
    .post('/backend_banco/logout/'+ newUserId)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que hace logout de usuario test: con token inválido", function(done){
    chai.request(url)
    .post('/backend_banco/logout/'+ newUserId)
    .set('Authorization',token + 'a')
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(403);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que hace logout de un uusario que no existe", function(done){
    chai.request(url)
    .post('/backend_banco/logout/10000000')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que hace logout de un uusario que no ha hecho login", function(done){
    chai.request(url)
    .post('/backend_banco/logout/1')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(401);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it("Test que hace logout de usuario test", function(done){
    chai.request(url)
    .post('/backend_banco/logout/'+ newUserId)
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que el objeto sea un json
        res.body.should.be.an('object');
        //Comprobamos la integridad del usuario
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )

}
)
describe("Test de API de usuarios: borrado de usuario",
function() {
it('Test que borra usuario no numérico: aaa' , function(done){
  chai.request(url)
  .delete('/backend_banco/users/aaa' )
  .set('Authorization',token)
  .end(
    function(err, res){
      // //función manejadora: es donde irán las aserciones
      console.log("Request finished");
      //chequeo el status de la respuesta
      res.should.have.status(400);
      //chequeo que el objeto sea un json
      res.body.should.be.an('object');
      //Comprobamos la integridad
      res.body.should.have.property('msg');
      done(); // momento en el que hay que comprobar las aserciones
    }
  )
}
)
it('Tests que borra usuario inexistente: 0', function(done){
  chai.request(url)
  .delete('/backend_banco/users/0')
  .set('Authorization',token)
  .end(
    function(err, res){
      // //función manejadora: es donde irán las aserciones
      console.log("Request finished");
      //chequeo el status de la respuesta
      res.should.have.status(404);
      //chequeo que el objeto sea un json
      res.body.should.be.an('object');
      //Comprobamos la integridad
      res.body.should.have.property('msg');
      done(); // momento en el que hay que comprobar las aserciones
    }
  )
}
)
it('Test que borra usuario test sin token', function(done){
  chai.request(url)
  .delete('/backend_banco/users/' + newUserId)
  .end(
    function(err, res){
      // //función manejadora: es donde irán las aserciones
      console.log("Request finished");
      //chequeo el status de la respuesta
      res.should.have.status(400);
      //chequeo que el objeto sea un json
      res.body.should.be.an('object');
      //Comprobamos la integridad
      res.body.should.have.property('msg');
      done(); // momento en el que hay que comprobar las aserciones
    }
  )
}
)
it('Test que borra usuario test con token inválido', function(done){
  chai.request(url)
  .delete('/backend_banco/users/' + newUserId)
  .set('Authorization',token + 'a')
  .end(
    function(err, res){
      // //función manejadora: es donde irán las aserciones
      console.log("Request finished");
      //chequeo el status de la respuesta
      res.should.have.status(403);
      //chequeo que el objeto sea un json
      res.body.should.be.an('object');
      //Comprobamos la integridad
      res.body.should.have.property('msg');
      done(); // momento en el que hay que comprobar las aserciones
    }
  )
}
)
it('Test que borra usuario test', function(done){
  chai.request(url)
  .delete('/backend_banco/users/' + newUserId)
  .set('Authorization',token)
  .end(
    function(err, res){
      // //función manejadora: es donde irán las aserciones
      console.log("Request finished");
      //chequeo el status de la respuesta
      res.should.have.status(200);
      //chequeo que el objeto sea un json
      res.body.should.be.an('object');
      //Comprobamos la integridad
      res.body.should.have.property('msg');
      done(); // momento en el que hay que comprobar las aserciones
    }
  )
}
)
}
)

describe("Test de API de conversor de divisas",
function() {
  it('Tests that returns currencies list', function(done){
    chai.request(url)
    .get('/backend_banco/currencies')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que la respuesta sea un array
        res.body.should.be.a('array');
        //Comprobamos la integridad de las cuentas
        for (currency of res.body){
          currency.should.have.property('currency');
          currency.should.have.property('currencyName');
        }
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Tests that returns user currency change of not existing currencies', function(done){
    chai.request(url)
    .get('/backend_banco/currencies/XXX/YYY/10')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(404);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Tests that returns user currency change of not numeric amount', function(done){
    chai.request(url)
    .get('/backend_banco/currencies/USD/EUR/aa')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(400);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('msg');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
  it('Tests that returns user currency change of existing currencies', function(done){
    chai.request(url)
    .get('/backend_banco/currencies/USD/EUR/10')
    .set('Authorization',token)
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo que devuelve un objeto
        res.body.should.be.an('object');
        //Comprobamos que devuelve un mensaje
        res.body.should.have.property('cambio');
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
  )
}
)
